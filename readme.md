# Banco BBVA Perú App

Implementar un Sistema para que los clientes puedan acceder y realizar cargos y abonos a sus cuentas.

## Comenzando 🚀

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

### Pre-requisitos 📋

Que cosas necesitas para instalar el software y como instalarlas.

Instalar node.
Instalar npm.
Instalar atom.

### Instalación 🔧

Crear un archivo .env para colocar los parámetros de conexión.

Ejecutar npm install

Finaliza con un ejemplo de cómo obtener datos del sistema o como usarlos para una pequeña demo

### Casos de Uso 🔧

Get

users/:id --> Devuelve los datos del usuario específico.

clients/:id --> Devuelve los datos del cliente específico.

accounts/:id --> Devuelve las cuentas del cliente específico.

movimientos/:id --> Devuelve los movimientos de una cuenta específica.

tipoCambio --> Devuelve el tipo de cambio del día.


Put

accounts/:id --> Actualizar el saldo y crea el movimiento.


Post

login --> Busca un usuario al autenticarse, colocando su usuario y contraseña.

logout --> Cierra sesión del usuario logeado.

## Despliegue 📦

Ejecutar npm run dev

## Construido con 🛠️

Menciona las herramientas que utilizaste para crear tu proyecto

* [nodejs](https://nodejs.org/es/) - El lenguaje del backend
* [npm](https://www.npmjs.com/package/download) - Administrador de dependencias
* [mongo](https://www.mongodb.com/cloud/atlas) - Base de datos

## Versionado 📌

Usamos [Bitbucket](https://bitbucket.org/) para el versionado. 

## Autores ✒️

Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios

* *Gilberto Meza* - Practitioner - (https://bitbucket.org/beto1627)
* *Roberto Ramírez* - Practitioner - (https://bitbucket.org/RobertoRRD/)

## Expresiones de Gratitud 🎁

* Gracias Ezequiel 📢