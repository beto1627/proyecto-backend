exports.MENSAJE_OK = 'Operación exitosa';
exports.MENSAJE_ERROR = 'Ocurrió un error inesperado';
exports.HTTP_STATUS_OK = 200;
exports.HTTP_STATUS_CREADO_OK = 201;
exports.HTTP_STATUS_ERROR_CONTROLADO = 204;
exports.HTTP_STATUS_ERROR_SERVER = 500;

exports.DIVISA_SOLES = 'PEN';
exports.DIVISA_DOLARES = 'USD';
exports.DIVISA_DOLARES = 'EUR';

exports.MENSAJE_CONEXION_EXITOSA = 'Successfully connected MongoDB';
exports.MENSAJE_USUARIO_NO_EXISTE = 'Usuario no existe';
exports.MENSAJE_CONTRASENA_INCORRECTA = 'Contraseña incorrecta';
exports.MENSAJE_USUARIO_NO_LOGEADO = 'Error en logout'
