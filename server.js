const express = require('express');
const body_parser = require('body-parser');
require('dotenv').config();//no almacenamos en una constante
const cors = require('cors');  // Instalar dependencia 'cors' con npm
const mongoose = require('mongoose');//conectarse a mongo
const app = express();
const port = process.env.PORT || 3000;
const request_json = require('request-json');
const URL_BASE = '/proyecto/v1/';
const URL_MONGO_DB = `mongodb+srv://${process.env.MDB_USER}:${process.env.MDB_PASSWORD}@${process.env.MDB_SERVER}/${process.env.MDB_DB}?retryWrites=true&w=majority`;
const URL_TIPO_CAMBIO = 'https://my-json-server.typicode.com/beto1627/demo/';

const util = require('./util.js');

const { Schema } = mongoose;
const schema_user = new Schema({
  id_user : Number,
  email : String,
  password : String,
  codigo_cliente : String,
  logged : Boolean
});
const schema_client = new Schema({
  codigo_cliente : String,
  type_document : String,
  document : String,
  first_name : String,
  last_name : String
});
const schema_account = new Schema({
  id_account : String,
  codigo_cliente : String,
  amount : Number,
  divisa : String,
  situacion: Number
});
const schema_movimiento = new Schema({
  id_account : String,
  date : String,
  divisa : String,
  amount: Number,
  flag_sit : Number
});

//CORS
app.use(cors());
app.options('*', cors());

app.listen(port, function(){
 console.log('Node JS escuchando en el puerto ' + port);
});

app.use(body_parser.json());

function mostrarError(error, response){
  console.log(error);
  response.status(util.HTTP_STATUS_ERROR_SERVER);
  response.send({'msn' : util.MENSAJE_ERROR});
}

function mostrarErrorControlado(mensaje, response){
  response.status(util.HTTP_STATUS_OK);
  response.send({'msn' : mensaje});
}

function obtenerFechaActual(){
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1;
  var yyyy = today.getFullYear();
  if(dd<10) {
      dd='0'+dd;
  }
  if(mm<10) {
      mm='0'+mm;
  }
  return dd+'/'+mm+'/'+yyyy;
}

//Petición GET de un usuario unico
app.get(URL_BASE + 'users/:id',
   function(request, response){
     let id = request.params.id;

     console.log(10-5);

     mongoose.connect(URL_MONGO_DB, function (error) {
         if (error){
           mostrarError(error, response);
           return;
         }
         console.log(util.MENSAJE_CONEXION_EXITOSA);

         const user = mongoose.model('users', schema_user);
         user.findOne({id_user : id},function(error, result){
           console.log('find usuario');
           if (error){
             mostrarError(error, response);
             return;
           }
           response.status(util.HTTP_STATUS_OK);
           response.send(result);
         });

     });
});

//Petición GET de un cliente unico
app.get(URL_BASE + 'clients/:id',
   function(request, response){
     let id = request.params.id;

     mongoose.connect(URL_MONGO_DB, function (error) {
         if (error){
           mostrarError(error, response);
           return;
         }
         console.log(util.MENSAJE_CONEXION_EXITOSA);

         const client = mongoose.model('clients', schema_client);
         client.findOne({codigo_cliente : id},function(error, result){
           console.log('find cliente');
           if (error){
             mostrarError(error, response);
             return;
           }
           response.status(util.HTTP_STATUS_OK);
           response.send(result);
         });

     });
});

//Petición GET de las cuentas de un cliente
app.get(URL_BASE + 'accounts/:id',
   function(request, response){
     let id = request.params.id;

     mongoose.connect(URL_MONGO_DB, function (error) {
         if (error){
           mostrarError(error, response);
           return;
         }
         console.log(util.MENSAJE_CONEXION_EXITOSA);

         const account = mongoose.model('accounts', schema_account);
         account.find({codigo_cliente : id},function(error, result){
           console.log('find cuentas');
           if (error){
             mostrarError(error, response);
             return;
           }
           response.status(util.HTTP_STATUS_OK);
           response.send(result);
         });

     });
});

//Petición GET de los movimientos de una cuenta
app.get(URL_BASE + 'movimientos/:id',
   function(request, response){
     let id = request.params.id;

     mongoose.connect(URL_MONGO_DB, function (error) {
         if (error){
           mostrarError(error, response);
           return;
         }
         console.log(util.MENSAJE_CONEXION_EXITOSA);

         const movimiento = mongoose.model('movimientos', schema_movimiento);
         movimiento.find({id_account : id},function(error, result){
           console.log('find movimientos');
           if (error){
             mostrarError(error, response);
             return;
           }
           response.status(util.HTTP_STATUS_OK);
           response.send(result);
         }).sort( { date : -1} );

     });
});

//Petición PUT a accounts
app.put(URL_BASE + 'accounts/:id',
   function(request, response){
     let id = request.params.id;

     mongoose.connect(URL_MONGO_DB, function (error) {
         if (error){
           mostrarError(error, response);
           return;
         }
         console.log(util.MENSAJE_CONEXION_EXITOSA);

         const account = mongoose.model('accounts', schema_account);
         account.findOne({id_account : id},function(error, account_upd){
           if (error){
             mostrarError(error, response);
             return;
           }

           const movimiento = mongoose.model('movimientos', schema_movimiento);
           var movimiento_new = new movimiento({
             id_account:  id,
             date: obtenerFechaActual(),
             divisa: account_upd.divisa,
             amount: request.body.amount,
             flag_sit: 0
           });

           movimiento_new.save(function(error, result) {
                 if (error){
                   mostrarError(error, response);
                   return;
                 }
            });

           account_upd.amount = (account_upd.amount + request.body.amount);

           account_upd.save(function(error, result) {

                 console.log('save');
                 if (error){
                   mostrarError(error, response);
                   return;
                 }
                 response.status(util.HTTP_STATUS_OK);
                 response.send({'msn' : util.MENSAJE_OK});
            });
         });

     });
});

//prueba
app.get(URL_BASE + 'tipoCambio',
   function(request, response){

     client = request_json.createClient(URL_TIPO_CAMBIO);
     let fechaCambio = obtenerFechaActual();
     let query = `fechaCambio=${fechaCambio}`;

     client.get('tipoCambio?' + query,
       function(error, res, body){
           if (error){
             mostrarError(error, response);
             return;
           }

           response.status(util.HTTP_STATUS_OK);
           response.send(body);
       });

});

//Method POST login
app.post(URL_BASE + "login",
  function (request, response){
    var request_email= request.body.email;
    var request_password= request.body.password;

    mongoose.connect(URL_MONGO_DB, function (error) {
        if (error){
          mostrarError(error, response);
          return;
        }
        console.log(util.MENSAJE_CONEXION_EXITOSA);

        const user = mongoose.model('users', schema_user);
        user.findOne({email : request_email},function(error, user_upd){
          console.log('find');
          if (error){
            mostrarError(error, response);
            return;
          }
          if(user_upd!=undefined){
              if (user_upd.password == request_password) {
                user_upd.logged = true;

                user_upd.save(function(error, result) {
                      console.log('save');
                      if (error){
                        mostrarError(error, response);
                        return;
                      }
                      response.status(util.HTTP_STATUS_OK);
                      response.send(result);
                });

              }else {
                mostrarErrorControlado(util.MENSAJE_CONTRASENA_INCORRECTA, response);
                return;
              }
          }else{
            mostrarErrorControlado(util.MENSAJE_USUARIO_NO_EXISTE, response);
            return;
          }
        });

    });

});

//Method POST logout
app.post(URL_BASE + "logout",
  function (request, response){
    var request_codigo_cliente= request.body.codigo_cliente;

    mongoose.connect(URL_MONGO_DB, function (error) {
        if (error){
          mostrarError(error, response);
          return;
        }
        console.log(util.MENSAJE_CONEXION_EXITOSA);


        const user = mongoose.model('users', schema_user);
        user.findOne({codigo_cliente : request_codigo_cliente, logged : true},function(error, user_upd){
          console.log('find');
          if (error){
            mostrarError(error, response);
            return;
          }

          if(user_upd!=undefined){
            user_upd.logged = false;

            user_upd.save(function(error, result) {
                  console.log('save');
                  if (error){
                    mostrarError(error, response);
                    return;
                  }
                  response.status(util.HTTP_STATUS_OK);
                  response.send(result);
            });

          }else{
            mostrarErrorControlado(util.MENSAJE_USUARIO_NO_LOGEADO, response);
            return;
          }

        });

      });
});
