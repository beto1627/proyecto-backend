#Imagen docker file
FROM node:latest

#crear directorio de trabajo del contenedor docker
WORKDIR /docker-dir-apitechuvx

#copiar archivos del proyecto en el directorio trabajo Docker
ADD . /docker-dir-apitechuvx

#instalar dependencias produccion del proyecto
#RUN npm install --only=production

#puerto donde exponemos contenedor
EXPOSE 3000

#comando lanzar app
CMD ["npm", "run", "prod"]
